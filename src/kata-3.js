export function kata3(n, m) {
    const squareDividers = [];
    for (let i = n; i <= m; i++) {
        if (
            pipe(
                getDividers,
                mapSquared,
                reduceSum,
                Math.sqrt,
                Number.isInteger
            )(i)
        ) {
            squareDividers.push([
                i,
                pipe(
                    getDividers,
                    mapSquared,
                    reduceSum
                )(i)
            ]);
        }
    }
    return squareDividers;
}

const pipe = (...fns) => x => fns.reduce((v, f) => f(v), x);

const isPrime = (number, i) => number % i === 0;

const getDividers = number => {
    const dividers = [1];
    for (let i = 2; i <= number / 2; i++) {
        if (isPrime(number, i)) {
            dividers.push(i);
        }
    }
    if (number !== 1) dividers.push(number);
    return dividers;
};

const mapSquared = numbers => numbers.map(number => number * number);

const sum = (a, b) => a + b;

const reduceSum = numbers => numbers.reduce(sum, 0);
