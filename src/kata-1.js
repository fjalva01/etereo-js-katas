export function kata1(number) {
    return parseInt(
        number
            .toString()
            .split("")
            .sort()
            .reverse()
            .join("")
    );
}
