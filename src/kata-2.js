export function kata2(obj, def, path) {
    return path
        ? get(obj, def, path)
        : function(path) {
              return get(obj, def, path);
          };
}

const get = (obj, def, path) =>
    path
        .split(".")
        .reduce(
            (accumulator, currentValue) =>
                accumulator && accumulator[currentValue]
                    ? accumulator[currentValue]
                    : def,
            obj
        );
